from flask import Flask, request, jsonify
import math

app = Flask(__name__)

@app.route('/', methods=['GET'])
def hello():
    return jsonify({'msg': 'Hola ISIS4426'})

@app.route('/sum', methods=['POST'])
def add():
    data = request.get_json()
    num1 = data['num1']
    num2 = data['num2']
    result = num1 + num2
    return jsonify({'result': result})

@app.route('/subtract', methods=['POST'])
def subtract():
    data = request.get_json()
    num1 = data['num1']
    num2 = data['num2']
    result = num1 - num2
    return jsonify({'result': result})

@app.route('/multiply', methods=['POST'])
def multiply():
    data = request.get_json()
    num1 = data['num1']
    num2 = data['num2']
    result = num1 * num2
    return jsonify({'result': result})

@app.route('/divide', methods=['POST'])
def divide():
    data = request.get_json()
    num1 = data['num1']
    num2 = data['num2']

    if num2 == 0:
        return jsonify({'error': 'Cannot divide by zero'})

    result = num1 / num2
    return jsonify({'result': result})

if __name__ == '__main__':
    app.run(debug=True)
