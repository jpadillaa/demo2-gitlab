# Demo Gitlab CI/CD

## Getting started
Es un proyecto de ejemplo para implementar un pipeline sencillo con gitlab-ci. Tenemos un API REST en Python y Flask como el pretexto. Además de un archivo de pruebas y un dockerfile que genera una imagen para nuestra aplicación.

El API REST proporciona los siguientes endpoints:

* GET /: Retorna un saludo en formato JSON.
* POST /sum: Suma dos números proporcionados en el cuerpo de la solicitud.
* POST /subtract: Resta el segundo número del primero proporcionado en el cuerpo de la solicitud.
* POST /multiply: Multiplica dos números proporcionados en el cuerpo de la solicitud.
* POST /divide: Divide el primer número por el segundo proporcionado en el cuerpo de la solicitud.

### Ejemplo de Solicitud
<code>curl -X POST http://127.0.0.1:5000/sum -H "Content-Type: application/json" -d '{"num1": 5, "num2": 3}'</code>

### Ejemplo de Respuesta
<code>{
  "result": 8
} </code>

## Instalación
Requisitos Previos:
* Python 3.x
* pip

Pasos de Instalación
<code>pip install flask</code>

## Iniciar el Servidor
Para iniciar el servidor, ejecuta el siguiente comando:
<code>flask run --host=0.0.0.0</code>

El servidor Flask se iniciará en http://127.0.0.1:5000/ por defecto.

## Pruebas
Para ejecutar las pruebas, asegúrate de haber instalado las dependencias según se indica en la sección de instalación. Ejecute el siguiente comando:
<code>python -m unittest test_api.py</code>

## Docker
Si prefiere ejecutar la aplicación en un contenedor Docker, puede hacerlo siguiendo estos pasos:
1. Asegúrase de tener Docker instalado en tu sistema.
2. Ejecute el siguiente comando para construir la imagen Docker.

<code>docker build -t appimage:latest .</code>

### Ejecutar el Contenedor Docker
Una vez que hayas construido la imagen Docker, puedes ejecutar un contenedor con la aplicación Flask utilizando el siguiente comando:

<code>docker run --name myapp -p 5000:5000 -e -d appimage</code>


