import unittest
import json
from flask import Flask, request
from app import app

class TestAPIEndpoints(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_add(self):
        data = {'num1': 5, 'num2': 3}
        response = self.app.post('/sum', json=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {'result': 8})

    def test_subtract(self):
        data = {'num1': 7, 'num2': 4}
        response = self.app.post('/subtract', json=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {'result': 3})

    def test_multiply(self):
        data = {'num1': 6, 'num2': 2}
        response = self.app.post('/multiply', json=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {'result': 12})

    def test_divide(self):
        data = {'num1': 10, 'num2': 2}
        response = self.app.post('/divide', json=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {'result': 5})

    def test_divide_by_zero(self):
        data = {'num1': 10, 'num2': 0}
        response = self.app.post('/divide', json=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {'error': 'Cannot divide by zero'})

if __name__ == '__main__':
    unittest.main()